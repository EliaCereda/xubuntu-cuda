FROM nvidia/cudagl:10.1-devel-ubuntu18.04

ENV NVIDIA_DRIVER_CAPABILITIES=all

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update && apt-get -y dist-upgrade

RUN apt-get install -y \
  xfce4 \
  xfce4-terminal

RUN apt-get install -y \
  cuda-nsight-compute-10-1 \
  libxkbcommon-x11-0

RUN apt-get install -y \
  aptitude \
  atop \
  firefox \
  libfftw3-dev \
  gdb \
  gettext-base \
  htop \
  less \
  lsof \
  nano \
  openssh-server \
  rsync \
  screen \
  strace \
  sudo \
  supervisor \
  uuid-runtime \
  wget

# RUN rm -rf /var/cache/apt /var/lib/apt/lists

# Ensure the OpenSSH keys are not hardcoded in the image
RUN cp -r /etc/ssh /ssh_orig && rm -rf /etc/ssh/*

# Add files
ADD bin /usr/bin
ADD etc /etc

# Install custom packages
WORKDIR /var/tmp
ADD packages packages
RUN bash -c 'dpkg -i packages/{turbovnc,xorg-server}/*.deb' && rm -rf packages

RUN wget https://cmake.org/files/v3.14/cmake-3.14.5-Linux-x86_64.sh && \
    sh cmake-3.14.5-Linux-x86_64.sh --prefix=/usr/local --exclude-subdir && \
    rm cmake-3.14.5-Linux-x86_64.sh

EXPOSE 22
WORKDIR /
ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]
CMD ["supervisord"]
