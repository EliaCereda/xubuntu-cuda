#!/bin/bash

set -e

NVIDIA_VERSION=418.39-0ubuntu1
NVIDIA_PACKAGES=(
  xserver-xorg-video-nvidia
  libnvidia-cfg1

  libnvidia-gl
  libnvidia-common
  libnvidia-compute

  nvidia-utils
)

NVIDIA_EXPECTED_VERSION=${NVIDIA_VERSION%-*}
NVIDIA_RUNTIME_VERSION=$(nvidia-smi -q | sed -n 's/Driver Version.*: \(.*\)/\1/p')

if [ "$NVIDIA_EXPECTED_VERSION" == "$NVIDIA_RUNTIME_VERSION" ]
then
  echo "Found NVIDIA driver $NVIDIA_RUNTIME_VERSION"
else
  echo "Expected NVIDIA driver version ${NVIDIA_EXPECTED_VERSION}, but found ${NVIDIA_RUNTIME_VERSION}"
  exit
fi

echo "Diverting the files mapped by the docker runtime..."

MAPPER_DEVICE="/dev/mapper/vgroot-lvroot"

mount | grep "${MAPPER_DEVICE}" | while read -r line; do
  array=($line)
  file_path="${array[2]}"

  dpkg-divert --add "$file_path"
done

echo "Installing NVIDIA packages..."

NVIDIA_MAJOR_VERSION=${NVIDIA_EXPECTED_VERSION%.*}
NVIDIA_MINOR_VERSION=${NVIDIA_EXPECTED_VERSION#*.}

NVIDIA_PACKAGES_MAJOR=(${NVIDIA_PACKAGES[@]/%/-$NVIDIA_MAJOR_VERSION})
NVIDIA_PACKAGES_PINNED=(${NVIDIA_PACKAGES_MAJOR[@]/%/=$NVIDIA_VERSION})

apt-get install ${NVIDIA_PACKAGES_PINNED[@]}
apt-mark hold ${NVIDIA_PACKAGES_MAJOR[@]}

echo "Configuring Xorg..."

NVIDIA_GPUS=($(ls /proc/driver/nvidia/gpus/))

# Convert to the format expected by Xorg
function xconfig_bus_id() {
  local OLD_IFS=$IFS
  IFS=":."

  local ID_COMPONENTS=($1)

  local DOMAIN=${ID_COMPONENTS[0]}
  local BUS=${ID_COMPONENTS[1]}
  local DEVICE=${ID_COMPONENTS[2]}
  local FUNC=${ID_COMPONENTS[3]}

  IFS=OLD_IFS

  if (( $DOMAIN != 0 ))
  then
    printf "PCI:%d@%d:%d:%d" 0x$BUS 0x$DOMAIN 0x$DEVICE 0x$FUNC
  else
    printf "PCI:%d:%d:%d" 0x$BUS 0x$DEVICE 0x$FUNC
  fi
}

export NVIDIA_BUS_ID=$(xconfig_bus_id ${NVIDIA_GPUS[0]})

envsubst < /etc/X11/xorg.conf.in > /etc/X11/xorg.conf
