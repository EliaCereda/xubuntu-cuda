#!/bin/bash

# Add sample user
# sample user uses uid 999 to reduce conflicts with user ids when mounting an existing home dir
# run `openssl passwd -1 'newpassword'` to create a custom hash
if [ ! $PASSWORDHASH ]; then
    export PASSWORDHASH='$1$sg3pGv2D$QMHdh.gneXn0quudQNZnV0'
fi

addgroup --gid 999 ubuntu && \
useradd -m -u 999 -s /bin/bash -g ubuntu ubuntu
echo "ubuntu:$PASSWORDHASH" | /usr/sbin/chpasswd -e
echo "ubuntu    ALL=(ALL) ALL" >> /etc/sudoers
unset PASSWORDHASH

# Configure openssh-server

if [ ! -f "/etc/ssh/sshd_config" ];
  then
    cp /ssh_orig/sshd_config /etc/ssh
fi

if [ ! -f "/etc/ssh/ssh_config" ];
  then
    cp /ssh_orig/ssh_config /etc/ssh
fi

if [ ! -f "/etc/ssh/moduli" ];
  then
    cp /ssh_orig/moduli /etc/ssh
fi

# generate fresh rsa key if needed
if [ ! -f "/etc/ssh/ssh_host_rsa_key" ];
  then 
    ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi

# generate fresh dsa key if needed
if [ ! -f "/etc/ssh/ssh_host_dsa_key" ];
  then 
    ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi

# Prepare run dir
mkdir -p /var/run/sshd

# Configure TurboVNC vncserver
# Setting an empty VNC password since the VNC server is only reachable from localhost
if [[ ! -d "/home/ubuntu/.vnc/" ]];
  then
    sudo -Hu ubuntu bash -c '\
      mkdir -p ~/.vnc && \
      /opt/TurboVNC/bin/vncpasswd -f < /dev/zero > ~/.vnc/passwd && \
      chmod 600 ~/.vnc/passwd
    '
fi

# generate machine-id
uuidgen > /etc/machine-id

# install NVIDIA drivers
install-nvidia.sh

# Execute CMD
exec "$@"
